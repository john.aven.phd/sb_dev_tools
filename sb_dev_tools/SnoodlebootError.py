

class SnoodlebootError(Exception):
    """
    Exception class for Snoodleboot development.
    """

    pass
