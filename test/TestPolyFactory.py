from unittest import TestCase

from test.test_packages.pf_tester import PF_Tester


class TestPolyFactory(TestCase):

    def setUp(self):
        self.pf = PF_Tester

    def tearDown(self):
        pass

    def test_find_factories(self):

        self.pf.discover_class_factories()
        print(self.pf.create(object_id='test.test_packages.TestMixin.mm').__class__)

    def test_find_python_modules(self):
        pass

